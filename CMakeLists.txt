cmake_minimum_required(VERSION 2.8)

project(Fsm404)

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
set(CMAKE_BUILD_RPATH ${CMAKE_BINARY_DIR}/lib)

set(CMAKE_CXX_FLAGS "-O2 -std=c++14")

set (Fsm404_VERSION_MAJOR 0)
set (Fsm404_VERSION_MINOR 1)

set (CMAKE_CXX_STANDARD 14)


find_package (Threads REQUIRED)

include_directories("include")

add_subdirectory(src)