#include "fsm404Timer.hpp"
#include "fsm404DispatchQueue.hpp"

#include <iostream>

using namespace std::chrono_literals;

class EventLoop
{
public:
	~EventLoop(){}

	static fsm404::DispatchQueue& Get() {
		static EventLoop el;
		return el.m_Queue;
	}

private:
	EventLoop() {}
	EventLoop(const EventLoop&) = delete;
	EventLoop(EventLoop&&) = delete;
	EventLoop& operator=(const EventLoop&) = delete;
	EventLoop& operator=(EventLoop&&) = delete;
	
private:
	fsm404::DispatchQueue m_Queue;
};

int main() {
	fsm404::Timer t1([](){std::cout << "t1" << std::endl;}, EventLoop::Get());
	fsm404::Timer t2([](){std::cout << "t2" << std::endl;}, EventLoop::Get());
	fsm404::Timer t3([](){std::cout << "t3" << std::endl;}, EventLoop::Get());

	t1.start(500ms);
	t2.start(1s);
	t3.oneshot(1500ms);

	std::this_thread::sleep_for(std::chrono::seconds(2));
	t1.stop();
	std::this_thread::sleep_for(std::chrono::seconds(2));

	return 0;
}