#include "fsm404Macro.hpp"
#include "fsm404DispatchQueue.hpp"

#include <iostream>

class EventLoop
{
public:
	~EventLoop(){}

	static EventLoop& Get() {
		static EventLoop el;
		return el;
	}

	void postEvent(fsm404::StateMachine &sm, std::shared_ptr<fsm404::event> e) {
		m_Queue.dispatch([&, e](){sm.processEvent(e);});
	}

private:
	EventLoop() {}
	EventLoop(const EventLoop&) = delete;
	EventLoop(EventLoop&&) = delete;
	EventLoop& operator=(const EventLoop&) = delete;
	EventLoop& operator=(EventLoop&&) = delete;
	
private:
	fsm404::DispatchQueue m_Queue;
};

int main() {
	fsm404Event(myevent12, fsm404::event);
	fsm404Event(myevent21, fsm404::event);
	fsm404Event(myevent34, fsm404::event);
	fsm404Event(myevent43, fsm404::event);

	fsm404State(s1);
	fsm404State(s2);
	fsm404State(s3);
	fsm404State(s4);

	fsm404ActionTransition(s1, myevent12, s2,
		[](std::shared_ptr<fsm404::event> e){
			std::cout << "s1 -> s2" << std::endl;
		});
	fsm404ActionTransition(s2, myevent21, s1,
		[](std::shared_ptr<fsm404::event> e){std::cout << "s2 -> s1" << std::endl;});
	fsm404ActionTransition(s3, myevent34, s4,
		[](std::shared_ptr<fsm404::event> e){
			std::cout << "s3 -> s4" << std::endl;
		});
	fsm404ActionTransition(s4, myevent43, s3,
		[](std::shared_ptr<fsm404::event> e){std::cout << "s4 -> s3" << std::endl;});

	fsm404::StateMachine sm;
	sm.addState(s1);
	sm.addState(s2);
	s2->addState(s3);
	s2->addState(s4);
	sm.setInitialState(s1);
	s2->setInitialState(s3);

	std::shared_ptr<myevent12> e12 = std::make_shared<myevent12>();
	std::shared_ptr<myevent21> e21 = std::make_shared<myevent21>();
	std::shared_ptr<myevent34> e34 = std::make_shared<myevent34>();
	std::shared_ptr<myevent43> e43 = std::make_shared<myevent43>();

	EventLoop::Get().postEvent(sm, e12);
	EventLoop::Get().postEvent(sm, e34);
	EventLoop::Get().postEvent(sm, e21);
	EventLoop::Get().postEvent(sm, e12);
	EventLoop::Get().postEvent(sm, e34);
	EventLoop::Get().postEvent(sm, e43);

	std::this_thread::sleep_for(std::chrono::seconds(1));

	return 0;
}