#include "fsm404Macro.hpp"

#include <iostream>

int main() {
	fsm404LoadedEvent(myevent, fsm404::event, bool);
	fsm404Event(myevent2, fsm404::event);

	fsm404State(s1);
	fsm404State(s2);

	fsm404GuardTransition(s1, myevent, s2,
		[](std::shared_ptr<fsm404::event> e){
			std::cout << "value: " << std::boolalpha <<
			static_cast<myevent*>(e.get())->value << std::endl;
			return static_cast<myevent*>(e.get())->value;
		});
	fsm404ActionTransition(s2, myevent2, s1,
		[](std::shared_ptr<fsm404::event> e){std::cout << "Hello!" << std::endl;});

	fsm404::ThreadedStateMachine sm;
	sm.addState(s1);
	sm.addState(s2);
	sm.setInitialState(s1);

	std::shared_ptr<myevent> e = std::make_shared<myevent>();
	e->value = false;

	sm.processEvent(e);
	sm.processEvent( std::make_shared<myevent2>() );
	e = std::make_shared<myevent>();
	e->value = true;
	sm.processEvent(e);
	sm.processEvent( std::make_shared<myevent2>() );

	std::this_thread::sleep_for(std::chrono::seconds(1));

	return 0;
}