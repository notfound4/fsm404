#include "fsm404Timer.hpp"

#include "fsm404DispatchQueue.hpp"

#include <iostream>

namespace fsm404 {

	Timer::Timer(const TimeoutFct &task, DispatchQueue &queue) : m_Task(task), m_Queue(queue), m_Running(false) {
		m_Info.id = this;
	}

	Timer::Timer(TimeoutFct &&task, DispatchQueue &queue) : m_Task(std::move(task)), m_Queue(queue), m_Running(false) {
		m_Info.id = this;
	}

	Timer::~Timer() {
		std::unique_lock<std::mutex> lock(m_Lock);
		m_Queue.removeTimer(m_Info);
		lock.unlock();
	}

	void Timer::oneshot(const std::chrono::milliseconds &interval) {
		std::unique_lock<std::mutex> lock(m_Lock);
		if (m_Running)
			throw timer_already_running();
		m_Info.timeout = ClockType::now() + interval;
		m_Running = true;
		m_Oneshot = true;
		m_Queue.addTimer(m_Info);
		lock.unlock();
	}

	void Timer::oneshot(std::chrono::milliseconds &&interval) {
		std::unique_lock<std::mutex> lock(m_Lock);
		if (m_Running)
			throw timer_already_running();
		m_Info.timeout = ClockType::now() + std::move(interval);
		m_Running = true;
		m_Oneshot = true;
		m_Queue.addTimer(m_Info);
		lock.unlock();
	}

	void Timer::start(const std::chrono::milliseconds &interval) {
		std::unique_lock<std::mutex> lock(m_Lock);
		if (m_Running)
			throw timer_already_running();
		m_Info.timeout = ClockType::now() + interval;
		m_Timeout = interval;
		m_Running = true;
		m_Oneshot = false;
		m_Queue.addTimer(m_Info);
		lock.unlock();
	}

	void Timer::start(std::chrono::milliseconds &&interval) {
		std::unique_lock<std::mutex> lock(m_Lock);
		if (m_Running)
			throw timer_already_running();
		m_Info.timeout = ClockType::now() + std::move(interval);
		m_Timeout = interval;
		m_Running = true;
		m_Oneshot = false;
		m_Queue.addTimer(m_Info);
		lock.unlock();
	}

	void Timer::stop() {
		std::unique_lock<std::mutex> lock(m_Lock);
		m_Queue.removeTimer(m_Info);
		lock.unlock();
	}

	void Timer::process() {
		std::unique_lock<std::mutex> lock(m_Lock);
		if (m_Oneshot){
			m_Running = false;
		} else {
			m_Info.timeout += m_Timeout;
			m_Queue.addTimer(m_Info);
		}
		m_Task();
		lock.unlock();
	}

}