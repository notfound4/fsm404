#include "fsm404.h"

#include <iostream>

namespace fsm404 {

	StateMachine::StateMachine() {
		m_InitialState = nullptr;
		m_CurrentState = nullptr;
	}

	void StateMachine::addState(State *s) {
		m_States.push_back(std::unique_ptr<State>(s));
	}

	void StateMachine::setInitialState(State *initial) {
		std::unique_lock<std::mutex> lock(m_Lock);
		m_InitialState = initial;
		m_CurrentState = initial;
		lock.unlock();
	}

	void StateMachine::processEvent(std::shared_ptr<event> e) {
		std::unique_lock<std::mutex> lock(m_Lock);
		if (m_CurrentState != nullptr) {
			State *res = m_CurrentState->processEventState(e);
			if (res != nullptr){
				m_CurrentState = res;
				m_CurrentState->onEntryInternal(e);
			}
		}
		lock.unlock();
	}


	ThreadedStateMachine::ThreadedStateMachine() { }

	ThreadedStateMachine::~ThreadedStateMachine() { }

	void ThreadedStateMachine::processEvent(std::shared_ptr<event> e) {
		m_Queue.dispatch([e, this](){StateMachine::processEvent(e);});
	}


	State::State() {
		m_Memorize = false;
	}

	State* State::processEventState(std::shared_ptr<event> e) {
		event *ev = e.get();
		if(m_Transitions.count(typeid(*ev).name()) == 0) {
			StateMachine::processEvent(e);
			return nullptr;
		}
		if ( std::get<2>(m_Transitions[typeid(*ev).name()]) &&
				!std::get<2>(m_Transitions[typeid(*ev).name()])(e) ) {
			StateMachine::processEvent(e);
			return nullptr;
		}
		onExitInternal(e);
		if (std::get<1>(m_Transitions[typeid(*ev).name()]))
			std::get<1>(m_Transitions[typeid(*ev).name()])(e);
		return std::get<0>(m_Transitions[typeid(*ev).name()]);
	}

	void State::onEntry(std::shared_ptr<event> e) {

	}

	void State::onExit(std::shared_ptr<event> e) {

	}

	void State::addTransition(std::string e, State *s, action a, guard g){
		m_Transitions[e] = std::tie(s, a, g);
	}

	void State::onEntryInternal(std::shared_ptr<event> e) {
		if (m_InitialState != nullptr) {
			if (not m_Memorize || m_CurrentState == nullptr ) {
				m_CurrentState = m_InitialState;
			}
			m_CurrentState->onEntryInternal(e);
		}
		onEntry(e);
	}

	void State::onExitInternal(std::shared_ptr<event> e) {
		if (m_CurrentState != nullptr) {
			m_CurrentState->onExitInternal(e);
		}
		onExit(e);
	}

}
