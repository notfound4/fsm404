#pragma once

#include <functional>
#include <list>
#include <map>
#include <memory>
#include <typeinfo>
#include <tuple>
#include <condition_variable>
#include <queue>
#include <mutex>
#include <thread>

#include "fsm404DispatchQueue.hpp"

namespace fsm404 {

	struct event {
		virtual void dummy(){}
	};

	typedef std::function<void(std::shared_ptr<event> e)> action;
	typedef std::function<bool(std::shared_ptr<event> e)> guard;

	class State;

	class StateMachine {
		using task = std::function<void(void)>;
	public:
		StateMachine();
		StateMachine(const StateMachine&) = delete;
		StateMachine(StateMachine&&) = delete;
		StateMachine& operator=(StateMachine &&) = delete;

		void addState(State *s);
		void setInitialState(State *initial);

		virtual void processEvent(std::shared_ptr<event> e);

	protected:
		std::list<std::unique_ptr<State>>	m_States;
		State							   *m_CurrentState;
		State							   *m_InitialState;
		std::mutex							m_Lock;

	};

	class ThreadedStateMachine : public StateMachine {
		using task = std::function<void(void)>;
	public:
		ThreadedStateMachine();
		~ThreadedStateMachine();

		virtual void processEvent(std::shared_ptr<event> e);

	private:
		DispatchQueue m_Queue;
	};

	class State : public StateMachine {
		friend class StateMachine;
	public:
		State();

		virtual State* processEventState(std::shared_ptr<event> e);

		virtual void onEntry(std::shared_ptr<event> e);
		virtual void onExit(std::shared_ptr<event> e);

		void addTransition(std::string e, State *s, action a = action(),
			guard g = guard());

	protected:
		void onEntryInternal(std::shared_ptr<event> e);
		void onExitInternal(std::shared_ptr<event> e);

	protected:
		bool														m_Memorize;
		std::map<std::string, std::tuple<State*, action, guard>>	m_Transitions;

	};

}