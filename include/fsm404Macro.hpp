#pragma once

#include "fsm404.hpp"

#include <type_traits>
#include <utility>

#define fsm404Event(name, super)											\
struct name : public super {												\
};																			\
static_assert(std::is_base_of<fsm404::event, name>::value,					\
	"Trying to create an event than does not derive from fsm404::event");

#define fsm404LoadedEvent(name, super, payload)								\
struct name : public super {												\
	payload value;															\
};																			\
static_assert(std::is_base_of<fsm404::event, name>::value,					\
	"Trying to create an event than does not derive from fsm404::event");

#define fsm404State(name)	\
fsm404::State *name = new fsm404::State();

#define fsm404Transition(src, e, dst)	\
src->addTransition(typeid(e).name(), dst);

#define fsm404ActionTransition(src, e, dst, act)	\
src->addTransition(typeid(e).name(), dst, act);

#define fsm404GuardTransition(src, e, dst, grd)	\
src->addTransition(typeid(e).name(), dst, fsm404::action(), grd);

#define fsm404ActionGuardTransition(src, e, dst, act, grd)	\
src->addTransition(typeid(e).name(), dst, act, grd);
