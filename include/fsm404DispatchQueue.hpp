#pragma once

#include "fsm404Timer.hpp"

#include <thread>
#include <functional>
#include <queue>
#include <mutex>
#include <condition_variable>

namespace fsm404 {

	class DispatchQueue {
		typedef std::function<void(void)> task;

		friend class Timer;

	public:
		DispatchQueue();
		~DispatchQueue();

	// dispatch and copy
		void dispatch(const task& op);
	// dispatch and move
		void dispatch(task&& op);

	// Deleted operations
		DispatchQueue(const DispatchQueue& rhs) = delete;
		DispatchQueue& operator=(const DispatchQueue& rhs) = delete;
		DispatchQueue(DispatchQueue&& rhs) = delete;
		DispatchQueue& operator=(DispatchQueue&& rhs) = delete;

	private:
		void addTimer(const Timer::TimerInfo &info);
		void removeTimer(const Timer::TimerInfo &info);

	private:
		std::mutex								m_Lock;
		std::thread								m_Thread;
		std::queue<task>						m_Queue;
		std::condition_variable					m_Cond;
		std::priority_queue<Timer::TimerInfo> 	m_Timers;
		bool									m_Quit = false;

		void handler(void);
	};

}