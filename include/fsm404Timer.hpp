#pragma once

#include <chrono>
#include <exception>
#include <functional>
#include <mutex>

struct timer_already_running : public std::exception {
	const char *what() const throw () {
		return "Timer is already running";
	}
};

namespace fsm404 {

	class DispatchQueue;

	class Timer {
		friend class DispatchQueue;
	public:
		using ClockType = std::chrono::steady_clock;
		using TimeType = ClockType::time_point;
		using TimeoutFct = std::function<void(void)>;

		Timer(const TimeoutFct &task, DispatchQueue &queue);
		Timer(TimeoutFct &&task, DispatchQueue &queue);
		~Timer();

		void oneshot(const std::chrono::milliseconds &interval);
		void oneshot(std::chrono::milliseconds &&interval);
		void start(const std::chrono::milliseconds &interval);
		void start(std::chrono::milliseconds &&interval);
		void stop();

		Timer() = delete;
		Timer(const Timer&) = delete;
		Timer(Timer&&) = delete;
		Timer &operator=(const Timer&) = delete;
		Timer &operator=(Timer&&) = delete;

		struct TimerInfo {
			TimeType timeout;
			Timer *id;
			friend bool operator<(const TimerInfo& l, const TimerInfo& r)
			{
				return l.timeout > r.timeout;
			}
		};

	private:
		void process();

	private:
		std::mutex			m_Lock;
		const TimeoutFct	m_Task;
		DispatchQueue  	   &m_Queue;
		TimerInfo			m_Info;
		bool				m_Running;
		bool				m_Oneshot;

		std::chrono::milliseconds	m_Timeout;
	};

}